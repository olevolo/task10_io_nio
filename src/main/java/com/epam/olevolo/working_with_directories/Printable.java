package com.epam.olevolo.working_with_directories;

import java.io.IOException;

@FunctionalInterface
public interface Printable {
    void print() throws IOException;
}
