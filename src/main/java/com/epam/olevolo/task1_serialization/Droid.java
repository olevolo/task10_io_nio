package com.epam.olevolo.task1_serialization;

import java.io.Serializable;

public class Droid implements Serializable {

    private String name;
    private int power;
    private int health;
    private transient boolean isAlive;

    public Droid(String name, int power, int health, boolean isAlive) {
        this.name = name;
        this.power = power;
        this.health = health;
        this.isAlive = isAlive;
    }

    String getName() {
        return name;
    }

    int getPower() {
        return power;
    }

    int getHealth() {
        return health;
    }

    boolean getIsAlive() {
        return isAlive;
    }
}
